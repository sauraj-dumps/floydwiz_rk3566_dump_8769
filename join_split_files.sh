#!/bin/bash

cat odm/bundled_persist-app/pcbtest/pcbtest.apk.* 2>/dev/null >> odm/bundled_persist-app/pcbtest/pcbtest.apk
rm -f odm/bundled_persist-app/pcbtest/pcbtest.apk.* 2>/dev/null
cat product/app/webview/webview.apk.* 2>/dev/null >> product/app/webview/webview.apk
rm -f product/app/webview/webview.apk.* 2>/dev/null
cat product/priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk.* 2>/dev/null >> product/priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk
rm -f product/priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk.* 2>/dev/null
cat system_ext/priv-app/PrimeBrowser/PrimeBrowser.apk.* 2>/dev/null >> system_ext/priv-app/PrimeBrowser/PrimeBrowser.apk
rm -f system_ext/priv-app/PrimeBrowser/PrimeBrowser.apk.* 2>/dev/null
cat bootimg/01_dtbdump_.* 2>/dev/null >> bootimg/01_dtbdump_
rm -f bootimg/01_dtbdump_.* 2>/dev/null
cat amfllampae.dtb.* 2>/dev/null >> amfllampae.dtb
rm -f amfllampae.dtb.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
