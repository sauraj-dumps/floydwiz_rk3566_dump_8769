#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_RK3566.mk

COMMON_LUNCH_CHOICES := \
    lineage_RK3566-user \
    lineage_RK3566-userdebug \
    lineage_RK3566-eng
