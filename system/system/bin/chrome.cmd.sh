#!/system/bin/sh

# Setup chrome command line
echo "chrome --user-agent=\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/100.0.3239.111 Safari/537.1\"" > /data/local/tmp/chrome-command-line
chmod 755 /data/local/tmp/chrome-command-line