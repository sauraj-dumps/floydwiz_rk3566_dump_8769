#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_RK3566.mk

COMMON_LUNCH_CHOICES := \
    omni_RK3566-user \
    omni_RK3566-userdebug \
    omni_RK3566-eng
