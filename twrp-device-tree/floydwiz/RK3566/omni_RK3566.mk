#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from RK3566 device
$(call inherit-product, device/floydwiz/RK3566/device.mk)

PRODUCT_DEVICE := RK3566
PRODUCT_NAME := omni_RK3566
PRODUCT_BRAND := Floydwiz
PRODUCT_MODEL := Primebook
PRODUCT_MANUFACTURER := floydwiz

PRODUCT_GMS_CLIENTID_BASE := android-google

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="RK3566-user 11 RP1A.201105.002 shen20230206 release-keys"

BUILD_FINGERPRINT := TOSCIDO/T201-EEA/T201-EEA:11/RP1A.201105.002/ysq20210322:user/release-keys
